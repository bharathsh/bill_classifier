import os 
import pandas as pd 
# Prepare a dataset with board names 
# Structure of the given datset 
# -Bill
#   -Board1
#     -board1image1
#     -board1image2
#   -Board2
#     -board2 image1

# Get the list of boards 
v = pd.Series(os.listdir('drive/My Drive/OCR/Bills/')).sort_values().reset_index(drop=True)

v = v.to_frame('board')
# Assign a key to the board 
v['key'] = v.groupby(v['board'].str.replace(' \(1\)','')).ngroup()+1
# Create an url for all the boards 
v['url'] = 'drive/My Drive/OCR/Bills/' + v['board']
# Save to pdf 
v.to_csv('drive/My Drive/OCR/Bills/keys.csv',index=None)

# For every board take url of all the images, with key as the index  
files = {}
for i,j,k in zip(v['board'],v['url'],v['key']):
  try:  
    files[k] = j+'/'+pd.Series(os.listdir(j))
  except:
    continue
    
# Convert the list of images to dataframe, assign the key     
main_df = pd.Series(files).apply(pd.Series).stack().reset_index(level=[0]).rename(columns={'level_0':'key',0:'img_urls'}).merge(v)

# Save all the images to a different directory
# Convert pdf to image and save all the images to '{name}_{class}.png'
import cv2
path = 'drive/My Drive/OCR/bill_image_dataset/'
from pdf2image import convert_from_path

    
def save_img_to_directory(k,url,i):
  '''
  Takes the key, url and index as an argument and saves the image to a specifc path 
  '''
  print(i)
  # Conver the pdf to image 
  if url[-3:] == 'pdf':
    pages = convert_from_path(url, 500)
    for page in pages:
      page.save(f'{i}.png', 'PNG')
    url = f'{i}.png'  
  # Save the image to pdf   
  img = cv2.imread(url)
  cv2.imwrite(f'{path}/{i}-{k}.png',img)

for k,url,i in zip(main_df['key'],main_df['img_urls'],main_df.index):
  save_img_to_directory(k,url,i)
  
# Data is stored in directory bill_image_dataset in the format (name_class.png)
# Example type 
#  '0-1.png',
#  '1-2.png',

# Create dataset 

path = 'drive/My Drive/OCR/bill_image_dataset/'
names = os.listdir(path)
dataset = pd.DataFrame({'names':names})
dataset['class'] = dataset['names'].str.extract('-(.*)\.')
dataset['url'] = path+dataset['names']

# Prepare the images and classes for training  
def prepareImages(data):
    print("Preparing images")
    X_train = np.zeros((len(data['url']), 150, 100, 3))
    count = 0
    
    for fig in data['url']:
        # Load images into images of size 100x100x3
        img = image.load_img(fig, target_size=(150, 100, 3))
        x = image.img_to_array(img)
        x = preprocess_input(x)

        X_train[count] = x
        if (count%10 == 0):
            print("Processing image: ", count+1, ", ", fig)
        count += 1
    
    return X_train

X_train = prepareImages(dataset)
oh_class = pd.get_dummies(dataset['class'])
y_train,y_keys = oh_class.values,oh_class.columns.tolist()

# Build the model - VGG with metrics 
from keras import backend as K

def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))
  
def get_model():
    model = Sequential()
    model.add(Conv2D(64, (3, 3), activation = "relu", input_shape = (150, 100, 3)))
    model.add(Dropout(0.625))    
    model.add(Conv2D(64, (3, 3), activation = "relu"))
    model.add(Dropout(0.625))
    model.add(Conv2D(64, (6, 6), activation = "relu"))
    model.add(Dropout(0.625))
    model.add(Conv2D(64, (9,9), activation = "relu"))
    model.add(Dropout(0.625))
    model.add(MaxPooling2D(pool_size = (2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation = 'relu'))
    model.add(Dropout(0.625))
    model.add(Dense(64, activation = 'relu'))
    model.add(BatchNormalization())
    model.add(Dense(y_train.shape[1], activation = 'sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc',f1_m,precision_m, recall_m])

    return model

# Get model 
model= get_model()
model.fit(X_train,y_train,epochs=10,batch_size=2,validation_split=0.1)

